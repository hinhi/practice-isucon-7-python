# coding: utf-8
import os
import MySQLdb
from PIL import Image
import numpy as np


SAVE_DIR = '../public/icons/'
config = {
    'db_host': os.environ.get('ISUBATA_DB_HOST', 'localhost'),
    'db_port': int(os.environ.get('ISUBATA_DB_PORT', '3306')),
    'db_user': os.environ.get('ISUBATA_DB_USER', 'root'),
    'db_password': os.environ.get('ISUBATA_DB_PASSWORD', ''),
}


def dbh():
    db = MySQLdb.connect(
        host=config['db_host'],
        port=config['db_port'],
        user=config['db_user'],
        passwd=config['db_password'],
        db='isubata',
        charset='utf8mb4',
        autocommit=True,
    )
    cur = db.cursor(cursorclass=MySQLdb.cursors.DictCursor,)
    cur.execute("SET SESSION sql_mode='TRADITIONAL,NO_AUTO_VALUE_ON_ZERO,ONLY_FULL_GROUP_BY'")
    return db


if __name__ == '__main__':
    cur = dbh().cursor()
    cur.execute("SELECT * FROM image")
    row = cur.fetchone()
    for row in cur.fetchall():
        with open(row[1], 'wb') as f:
            f.write(row[2])
